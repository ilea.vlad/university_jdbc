package org.SDA;

import java.sql.*;
import java.util.Scanner;

public class App {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/university_sda_10";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "admin";
    private static Connection connection;

    public static void main(String[] args) throws SQLException {
        Scanner keyboard = new Scanner(System.in);
        connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
        getProfesors();
        System.out.println("-----------------------------------------------------");
        getCourses();
        System.out.println("-----------------------------------------------------");
        insertDepartment();
        System.out.println("-----------------------------------------------------");
        updateCurs();
        System.out.println("-----------------------------------------------------");
        studentsFromSection();
        System.out.println("-----------------------------------------------------");
        updateCurs("Optional", "Fizica aplicata");
        System.out.println("-----------------------------------------------------");
        getCourseByName(keyboard.nextLine());
        System.out.println("-----------------------------------------------------");
        saveStudentInTransaction("Test transaction unsucces",100,1);



        connection.close();
    }

    private static void getCourses() throws SQLException {
        String sql = "select * from curs ";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            int id = resultSet.getInt("id_curs");
            String name = resultSet.getString("nume");
            int credits = resultSet.getInt("credite");
            String description = resultSet.getNString("descriere");
            int department_id = resultSet.getInt("id_departamanet");
            System.out.println(id + " " + name + " " + credits + " " + description + " " + department_id);
        }
        resultSet.close();
        statement.close();
    }

    private static void getProfesors() throws SQLException {
        String sql = "select profesor.id_profesor,profesor.nume,profesor.adresa,department.name,profesor.salariu,profesor.data_angajare from profesor join department on profesor.id_departament = department.id_department ";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            int id = resultSet.getInt("id_profesor");
            String name = resultSet.getString("nume");
            String adresa = resultSet.getString("adresa");
            String nume_departament = resultSet.getString("name");
            int salariu = resultSet.getInt("salariu");
            String data_angajare = resultSet.getString("data_angajare");
            System.out.println(id + " " + name + " " + adresa + " " + nume_departament + " " + salariu + " " + data_angajare);
        }
        resultSet.close();
        statement.close();
    }

    private static void insertDepartment() throws SQLException {
        String sql = "insert into department(name) values ('Java')";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);

        statement.close();
    }

    private static void updateCurs() throws SQLException {
        String sql = "update curs set descriere = 'Obligatoriu' where nume = 'SQL'";
        Statement statement = connection.createStatement();
        statement.executeUpdate(sql);
        statement.close();
    }

    private static void studentsFromSection() throws SQLException {
        String sql = "select sectie.nume,count(student_sectie.id_student) as 'numar studenti' from student join student_sectie on student.id_student = student_sectie.id_student join sectie on student_sectie.id_sectie = sectie.id_sectie group by sectie.nume";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            String nume = resultSet.getString("nume");
            int numar = resultSet.getInt("numar studenti");
            System.out.println(nume + " " + numar);
        }
        resultSet.close();
        statement.close();
    }

    private static void updateCurs(String description, String courseName) throws SQLException {
        String sql = "update curs set descriere =? where nume = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, description);
        preparedStatement.setString(2, courseName);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    private static void getCourseByName(String name) throws SQLException {
        String sql = "select * from curs where nume =?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, name);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            int id_curs = resultSet.getInt("id_curs");
            String nume = resultSet.getString("nume");
            int credite = resultSet.getInt("credite");
            String descriere = resultSet.getString("descriere");
            int id_departament = resultSet.getInt("id_departamanet");
            System.out.println(id_curs + " " + nume + " " + credite + " " + descriere + " " + id_departament);
        }
        resultSet.close();
        preparedStatement.close();
    }
    private static void saveStudentInTransaction(String name,int profesorId,int sectinId) {
        String studentSql = "insert into student(nume,id_profesor) values(?,?)";

        try {
            connection.setAutoCommit(false);
            PreparedStatement studentStatement = connection.prepareStatement(studentSql,Statement.RETURN_GENERATED_KEYS);
            studentStatement.setString(1,name);
            studentStatement.setInt(2,profesorId);
            studentStatement.executeUpdate();
            ResultSet resultSet = studentStatement.getGeneratedKeys();
            resultSet.next();

            int studentId = resultSet.getInt(1);


            resultSet.close();
            studentStatement.close();

            String sectionSQL = "insert into student_sectie(id_sectie,id_student) values(?,?)";
            PreparedStatement sectionStatement = connection.prepareStatement(sectionSQL);
            sectionStatement.setInt(1,sectinId);
            sectionStatement.setInt(2,studentId);
            sectionStatement.executeUpdate();
            resultSet.close();
            sectionStatement.close();
            connection.commit();
            connection.setAutoCommit(true);
        }catch (SQLException e){
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        }


    }

}
